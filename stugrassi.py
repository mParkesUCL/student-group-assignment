#!/usr/bin/env python3

"""
stugrassi.py - Assign students to groups according to their preferences

Copyright (c) 2020-2021, Philipp Maierhöfer <particle@maierhoefer.net>

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

import sys
import argparse
import collections
import math
import os

try:
    from ortools import __version__ as ortools_version
    from ortools.linear_solver import pywraplp
except:
    print("Failed importing the Google OR-Tools module.")
    print("OR-Tools can be installed with `pip3 install ortools`.")
    sys.exit(1)

# If fewer preferences than this value have been given,
# treat all non-preferred groups with preference position len(prefs).
# Set to 1 to disable, so that only empty prefs are padded.
# This can be understood as a minimal number of preferences that the students
# must give. It ensures that noone gets an advantage by giving fewer
# preferences.
pad_if_fewer_prefs_than = 3

# The cost to assign a student to a group that he didn't choose.
# This only applies if at least pad_if_fewer_prefs_than preferences are given.
# Set to None or to 0 to disable. In that case, such assignments are impossible
# (which may lead to an unsolvable matching game).
disfavoured_cost = 1000

team_bonus = 0.001

# The minimal increment to add to min_size to automatically calculate max_size.
# I.e. max_size >= min_size + minimal_maxsize_increment even if there are
# sufficient slots with a smaller increment.
minimal_maxsize_increment = 1

# In OR-Tools 7 CreateSolver() has a different signature than in version 8.
# Earlier version have not been checked.
create_solver_args = ["CBC"]
try:
    if int(ortools_version.split(".")[0]) < 8:
        create_solver_args = ["student_group_assignment"] + create_solver_args
except:
    # Just use the default.
    pass


class InputError(Exception):
    pass


class CostError(Exception):
    pass


def rank_to_cost(rank, n_groups):
    """rank is the position in the preference list,
    starting with 0 for the top choice."""
    if cost_function == "linear":
        cost = rank
    elif cost_function == "square":
        cost = int((rank * (rank + 1)) / 2)
    elif cost_function == "exp":
        cost = 2**rank
    elif cost_function == "decell":
        cost = 0
        for r in range(rank):
            cost = cost + n_groups - r - 1
    else:
        CostError("Unknown cost function: {}".format(cost_function))
    return cost


class _Group:
    def __init__(self, name, min_size=None, max_size=None):
        self.name = name
        self.min_size = min_size
        self.max_size = max_size
        self.first_choice = 0
        self.any_choice = 0
        if not min_size is None and min_size < 0:
            raise argparse.ArgumentTypeError(
                "Negative minimal group size: " + min_size
            ) from None
        if not max_size is None and max_size < 0:
            raise argparse.ArgumentTypeError(
                "Negative maximal group size: " + max_size
            ) from None
        if not min_size is None and not max_size is None and min_size > max_size:
            raise InputError(
                "Minimal group size must not be larger " "than the maximal group size."
            )

    def copy(self):
        cp = self.__class__(self.name, self.min_size, self.max_size)
        cp.first_choice = self.first_choice
        cp.any_choice = self.any_choice
        return cp


class Groups:
    def __init__(self, names_sizes=[], default_min_max=(None, None), final=False):
        self._groups = []
        self._groups_dict = {}  # {name: position in _groups}
        # default_min_max: forced min/max sizes for groups with
        # no explicitly given min/max size.
        self.default_min_size = default_min_max[0]
        self.default_max_size = default_min_max[1]
        if (
            not default_min_max[0] is None
            and not default_min_max[1] is None
            and default_min_max[0] > default_min_max[1]
        ):
            raise InputError(
                "Minimal default group size must not be larger "
                "than the maximal group size."
            )
        for ns in names_sizes:
            min_size = None
            max_size = None
            if isinstance(ns, str):
                name = ns
            else:
                name = ns[0]
                if len(ns) >= 2:
                    min_size = ns[1]
                if len(ns) >= 3:
                    max_size = ns[2]
            if name in self._groups_dict:
                raise InputError(
                    'A group with the name "' + name + '" has already been defined.'
                )
            self.add(name, min_size, max_size)
        self.final = final

    def add(self, name, min_size=None, max_size=None):
        if min_size is None and not self.default_min_size is None:
            min_size = self.default_min_size
        if max_size is None and not self.default_max_size is None:
            max_size = self.default_max_size
        self._groups_dict[name] = len(self._groups)
        self._groups.append(_Group(name, min_size, max_size))
        return self._groups[-1]

    def get(self, key):
        if isinstance(key, int):
            return self._groups[key]
        if not key in self._groups_dict:
            if self.final:
                raise InputError('Group "' + key + '" has not been defined.')
            self.add(key)
        return self._groups[self._groups_dict[key]]

    def position(self, name):
        return self._groups_dict[name]

    def __len__(self):
        return len(self._groups)

    def __iter__(self):
        return iter(self._groups)

    def names(self):
        return self._groups_dict.keys()

    def assign_sizes(self, total):
        total_minsize = 0
        total_maxsize = 0
        groups_auto_minsize = []
        groups_auto_maxsize = []
        groups_forced_maxsize = []
        for grp in self:
            if not grp.min_size is None:
                total_minsize += grp.min_size
            if not grp.max_size is None:
                total_maxsize += grp.max_size
            if grp.min_size is None:
                groups_auto_minsize.append(grp.name)
            if grp.max_size is None:
                groups_auto_maxsize.append(grp.name)
            else:
                groups_forced_maxsize.append(grp.name)

        if total_minsize > total:
            # More slots that must be filled than students.
            raise InputError(
                "Minimal group sizes imply more slots than there are students."
            )
        if not groups_auto_maxsize and total_maxsize < total:
            # All groups have forced max size. Need sufficient slots.
            raise InputError(
                "Forced maximal group sizes are insufficient "
                "for the number of students."
            )

        if groups_auto_minsize:
            # Distribute the slots evenly over all groups of unknown min_size.
            # If max_size is given, never set min_size larger than that.
            auto_minsize = int((total - total_minsize) / len(groups_auto_minsize))
            if auto_minsize < 0:
                auto_minsize = 0
            for grpname in groups_auto_minsize:
                maxsz = self.get(grpname).max_size
                if not maxsz is None and auto_minsize > maxsz:
                    self.get(grpname).min_size = maxsz
                    total_minsize += maxsz
                    groups_auto_minsize.remove(grpname)
            auto_minsize = int((total - total_minsize) / len(groups_auto_minsize))
            if auto_minsize < 0:
                auto_minsize = 0
            for grpname in groups_auto_minsize:
                self.get(grpname).min_size = auto_minsize
                total_minsize += auto_minsize

        if groups_auto_maxsize:
            # Calculate the total number of slots that must be added.
            # Use the same increment on top of min_size for each group
            # of unknown max_size.
            total_max_inc = total - total_minsize
            for grpname in groups_forced_maxsize:
                total_max_inc += self.get(grpname).min_size
                total_max_inc -= self.get(grpname).max_size
            if total_max_inc < 0:
                total_max_inc = 0
            max_inc = math.ceil(total_max_inc / len(groups_auto_maxsize))
            if max_inc < minimal_maxsize_increment:
                max_inc = minimal_maxsize_increment
            for grpname in groups_auto_maxsize:
                self.get(grpname).max_size = self.get(grpname).min_size + max_inc

    def copy(self):
        cp = Groups()
        for grp in self._groups:
            cp._groups.append(grp.copy())
        cp._groups_dict = self._groups_dict.copy()
        cp.default_min_size = self.default_min_size
        cp.default_max_size = self.default_max_size
        cp.final = self.final
        return cp


class Student:
    def __init__(self, sid, firstname, lastname, preferences, partner_sid=None):
        self.sid = sid
        self.firstname = firstname
        self.lastname = lastname
        self.preferences = preferences
        self.remove_duplicate_preferences()
        self.partner_sid = partner_sid
        if partner_sid == sid:
            raise InputError("Student " + sid + "can't be his own partner.")

    def remove_duplicate_preferences(self):
        seen = set()
        self.preferences = [
            p for p in self.preferences if not (p in seen or seen.add(p))
        ]

    @staticmethod
    def parse(spec):
        data = [dat.strip() for dat in spec.split(";")]
        if len(data) < 3:
            raise InputError(
                'Invalid preference data: missing student ID in "' + spec + '"'
            )
        lastname = data[0]
        firstname = data[1]
        sid = data[2]
        preferences = []
        if len(data) > 3:
            preferences = [p.strip() for p in data[3].split("|") if p.strip()]
        partner_sid = None
        if len(data) > 4 and data[4]:
            partner_sid = data[4]
        if not sid:
            raise InputError(
                'Invalid preference data: empty student ID in "' + spec + '"'
            )
        return Student(sid, firstname, lastname, preferences, partner_sid)


class Students:
    def __init__(self, students, groups):
        self._groups = groups.copy()
        self._students = []
        self._students_dict = {}  # {sid: position in _students}
        self._teams = []
        self._immutable = False
        for stud in students:
            self.add(stud)

    @staticmethod
    def parse(preferences_file, groups):
        students = []
        with open(preferences_file) as fh:
            for line in fh:
                spec = line.split("#", 1)[0].strip()
                if not spec:
                    continue
                students.append(Student.parse(spec))
        return Students(students, groups)

    def add(self, stud):
        if self._immutable:
            raise InputError(
                "Adding students is no more possible after "
                "auto-assigning the group sizes."
            )
        if stud.sid in self._students_dict:
            raise InputError('Duplicate student ID "' + stud.sid + '"')
        self._students_dict[stud.sid] = len(self._students)
        self._students.append(stud)
        if not stud.partner_sid is None:
            self._teams.append((stud.sid, stud.partner_sid))
        # Create groups from preferences if necessary.
        for pos, grpname in enumerate(stud.preferences):
            grp = self._groups.get(grpname)
            if pos == 0:
                grp.first_choice += 1
            grp.any_choice += 1

    def get(self, key):
        if isinstance(key, int):
            return self._students[key]
        return self._students[self._students_dict[key]]

    def position(self, sid):
        return self._students_dict[sid]

    def __len__(self):
        return len(self._students)

    def __iter__(self):
        return iter(self._students)

    def get_group(self, key):
        return self._groups.get(key)

    def count_groups(self):
        return len(self._groups)

    def group_position(self, name):
        return self._groups.position(name)

    def _assign_group_sizes(self):
        if self._immutable:
            return
        self._immutable = True
        self._groups.assign_sizes(len(self))
        for grp in self._groups:
            if grp.min_size > grp.any_choice:
                print(
                    "Group {} has minimal size {}, but is only a preference "
                    "of {} student(s).".format(grp.name, grp.min_size, grp.any_choice)
                )
            if not disfavoured_cost:
                InputError('You must set "disfavoured_cost" to find a match.')

    def get_teams(self):
        """Check that team partners exist and teams are mutual.
        Return list of teams in terms of student numbers.
        """
        for team in self._teams:
            if not team[1] in self._students_dict:
                print("Team partner does not exist:", team[1])
        sorted_teams = [tuple(sorted(t)) for t in self._teams]
        teams_tally = collections.Counter(sorted_teams)
        verified_teams = []
        for team, count in teams_tally.items():
            if count == 2:
                verified_teams.append(
                    tuple(sorted([self.position(team[0]), self.position(team[1])]))
                )
            else:
                print("Unmatched team:", team)
        self.verified_teams = sorted(verified_teams)
        return self.verified_teams

    def _get_mapped_group_sizes(self, group_map):
        # group_map = {from_groupname: to_groupname, ...}
        self._assign_group_sizes()
        # Check that all group names in group_map are valid.
        for from_grpname, to_grpname in group_map.items():
            if from_grpname not in self._groups.names():
                raise InputError("Invalid name in group map: " + from_grpname)
            if to_grpname not in self._groups.names():
                raise InputError("Invalid name in group map: " + to_grpname)

        # Reduced set of group names after group mappings:
        groupnames_set = set()
        for grp in self._groups:
            if not grp.name in group_map:
                groupnames_set.add(grp.name)

        # group_sizes = {group_number: (min_size, max_size), ...} after mapping.
        # Use a dict, because after mapping, group numbers are
        # not necessarily contiguous.
        group_sizes = {}
        for grpname in groupnames_set:
            grp = self.get_group(grpname)
            group_sizes[self.group_position(grpname)] = (grp.min_size, grp.max_size)
        for from_grpname, to_grpname in group_map.items():
            to_minmax = group_sizes[self.group_position(to_grpname)]
            from_grp = self.get_group(from_grpname)
            mapped_minmax = (
                to_minmax[0] + from_grp.min_size,
                to_minmax[1] + from_grp.max_size,
            )
            group_sizes[self.group_position(to_grpname)] = mapped_minmax

        return groupnames_set, group_sizes

    def _get_student_preference_cost(self, groupnames_set, group_map):
        # student_preference_cost[student_number] = [
        #     (group_number,rank,cost), ...]
        student_preference_cost = []
        for stud in self._students:
            prefcost = []
            # Padding is activated based on unmapped groups.
            pad = len(stud.preferences) < pad_if_fewer_prefs_than
            seen = set()
            rank = 0
            for grpname in stud.preferences:
                if grpname in group_map:
                    grpname = group_map[grpname]
                if grpname in seen:
                    continue
                prefcost.append(
                    (
                        self.group_position(grpname),
                        rank,
                        rank_to_cost(rank, len(groupnames_set)),
                    )
                )
                seen.add(grpname)
                rank = rank + 1
            if pad:
                for grpname in groupnames_set - seen:
                    prefcost.append(
                        (
                            self.group_position(grpname),
                            "padded " + str(rank),
                            rank_to_cost(rank, len(groupnames_set)),
                        )
                    )
            elif disfavoured_cost:
                for grpname in groupnames_set - seen:
                    prefcost.append(
                        (self.group_position(grpname), "disfavoured", disfavoured_cost)
                    )
            student_preference_cost.append(prefcost)
        return student_preference_cost

    def _get_team_data(self, student_preference_cost):
        # [(grpnum, studnum_1, rank_1, cost_1, studnum_2, rank_2, cost_2), ...]
        team_data = []
        for studnum_1, studnum_2 in self.get_teams():
            for grpnum_1, rank_1, cost_1 in student_preference_cost[studnum_1]:
                for grpnum_2, rank_2, cost_2 in student_preference_cost[studnum_2]:
                    if grpnum_1 == grpnum_2:
                        team_data.append(
                            (
                                grpnum_1,
                                studnum_1,
                                rank_1,
                                cost_1 - team_bonus,
                                studnum_2,
                                rank_2,
                                cost_2 - team_bonus,
                            )
                        )
        return team_data

    def get_solver_input(self, group_map):
        groupnames_set, group_sizes = self._get_mapped_group_sizes(group_map)
        student_preference_cost = self._get_student_preference_cost(
            groupnames_set, group_map
        )
        team_data = self._get_team_data(student_preference_cost)
        return group_sizes, student_preference_cost, team_data


class Solver:
    def __init__(self, students, group_map):
        # # this is messy but allows flask to pass things, will need larger rewrite to remove global dependences
        # disfavoured_cost = options["disfavoured_cost"]
        # pad_if_fewer_prefs_than = options["pad_if_fewer_prefs_than"]
        # team_bonus = options["team_bonus"]
        # cost_function = options["cost_function"]
        self._solved = False
        self._students = students
        solver_input = students.get_solver_input(group_map)
        self.group_sizes = solver_input[0]
        self.student_preference_cost = solver_input[1]
        self.team_data = solver_input[2]

        # Create the solver, add objective and contraints
        self.solver = pywraplp.Solver.CreateSolver(*create_solver_args)
        self.objective = self.solver.Objective()

        # A list of dicts containing the student--group variables and
        # corresponding preference ranks.
        # variables_rank[studnum] = {grpnum: (rank, [var1, var2, ...]), ...}
        self.variables_rank = []

        # student_constraints[studnum] is the constraint that the student
        # with number studnum is assigned to exactly one group.
        student_constraints = []
        for _ in self.student_preference_cost:
            student_constraints.append(self.solver.Constraint(1, 1))

        # group_constraints = {grpnum: constr}, where constr is the constraint
        # that group number grpnum has exactly group_sizes[grpnum] members.
        group_constraints = {}
        for grpnum, minmax_size in self.group_sizes.items():
            group_constraints[grpnum] = self.solver.Constraint(
                minmax_size[0], minmax_size[1]
            )

        for studnum, grpnum_rank_cost_ls in enumerate(self.student_preference_cost):
            self.variables_rank.append({})
            for grpnum, rank, cost in grpnum_rank_cost_ls:
                var = self.solver.IntVar(0.0, 1.0, "x_{}_{}".format(studnum, grpnum))
                self.variables_rank[-1][grpnum] = (rank, [var])
                student_constraints[studnum].SetCoefficient(var, 1)
                self.objective.SetCoefficient(var, cost)
                group_constraints[grpnum].SetCoefficient(var, 1)

        # Create team variables and constraints.
        # Instead of via variable x_s_g, student s can enter group g
        # via t_s_p_g with partner p at a price reduced by team_bonus.
        # This is only possible if both team members enter the same group
        # (constraint t_s_p_g - t_p_s_g = 0, i.e. either both in or both out).
        for (
            grpnum,
            studnum_1,
            rank_1,
            cost_1,
            studnum_2,
            rank_2,
            cost_2,
        ) in self.team_data:
            teamvar_1 = self.solver.IntVar(
                0.0, 1.0, "t_{}_{}_{}".format(studnum_1, studnum_2, grpnum)
            )
            teamvar_2 = self.solver.IntVar(
                0.0, 1.0, "t_{}_{}_{}".format(studnum_2, studnum_1, grpnum)
            )
            student_constraints[studnum_1].SetCoefficient(teamvar_1, 1)
            student_constraints[studnum_2].SetCoefficient(teamvar_2, 1)
            group_constraints[grpnum].SetCoefficient(teamvar_1, 1)
            group_constraints[grpnum].SetCoefficient(teamvar_2, 1)
            team_constraint = self.solver.Constraint(0, 0)
            team_constraint.SetCoefficient(teamvar_1, 1)
            team_constraint.SetCoefficient(teamvar_2, -1)
            self.objective.SetCoefficient(teamvar_1, cost_1)
            self.objective.SetCoefficient(teamvar_2, cost_2)
            rank_vars_1 = self.variables_rank[studnum_1][grpnum]
            assert rank_vars_1[0] == rank_1
            rank_vars_1[1].append(teamvar_1)
            rank_vars_2 = self.variables_rank[studnum_2][grpnum]
            assert rank_vars_2[0] == rank_2
            rank_vars_2[1].append(teamvar_2)

    def solve(self):
        if self._solved:
            return
        self.objective.SetMinimization()
        status = self.solver.Solve()
        if status != pywraplp.Solver.OPTIMAL:
            raise InputError("The problem does not have an optimal solution.")
        self._solved = True

        # Retrieve the solution and bring it into a usable format:
        #   group_members = {grpnum: [(studnum1,rank1), ...], ...}
        #   preferences_fulfilled = {rank: [first_choices, of which padded]}
        #   accepted_teams = set([(studnum1,studnum2), ...])
        self.group_members = {}
        for grpnum in self.group_sizes:
            self.group_members[grpnum] = []
        self.preferences_fulfilled = collections.defaultdict(lambda: [0, 0])
        self.accepted_teams = set()
        for studnum, rank_variables in enumerate(self.variables_rank):
            for grpnum, rank_vars in rank_variables.items():
                rank = rank_vars[0]
                for var in rank_vars[1]:
                    if var.solution_value() == 1:
                        self.group_members[grpnum].append((studnum, rank))
                        if isinstance(rank, str) and rank.startswith("padded "):
                            rank = int(rank[7:])
                            self.preferences_fulfilled[rank][1] += 1
                        self.preferences_fulfilled[rank][0] += 1
                        if var.name().startswith("t"):
                            team = map(int, var.name().split("_")[1:3])
                            self.accepted_teams.add(tuple(sorted(team)))

    def print_to_file(self):
        """
        Print to an output file


        """

    def print_result(self):
        with open("out.csv", "w") as outfile:
            serve_strp = ""
            serve_strs = ""
            p = "Supervisor, Surname, Forename, ID, Preference \n"
            serve_strp = serve_strp + p
            outfile.write(p)
            self.solve()
            firstname_maxlen = 0
            lastname_maxlen = 0
            sid_maxlen = 0
            # team_partners = {studnum: (partnernum, accepted), ...}
            team_partners = {}
            for team in self._students.verified_teams:
                accepted = team in self.accepted_teams
                team_partners[team[0]] = (team[1], accepted)
                team_partners[team[1]] = (team[0], accepted)
            for stud in self._students:
                firstname_maxlen = max(firstname_maxlen, len(stud.firstname))
                lastname_maxlen = max(lastname_maxlen, len(stud.lastname))
                sid_maxlen = max(sid_maxlen, len(stud.sid))
            for grp, grpnum, members in sorted(
                [
                    (self._students.get_group(grpnum), grpnum, len(stud_rank_ls))
                    for grpnum, stud_rank_ls in self.group_members.items()
                ],
                key=lambda el: el[0].name,
            ):
                s = (
                    "Group {}: {}..{} slots, {} members ({} first choices, "
                    "{} in total)"
                ).format(
                    grp.name,
                    *self.group_sizes[grpnum],
                    members,
                    grp.first_choice,
                    grp.any_choice,
                )
                serve_strs = serve_strs  + "\n" + (len(s) * "-") + "\n" + s
                p = "{0},".format(grp.name)
                #serve_strp = serve_strp + p
                print(s)
                print(len(s) * "-")
                serve_strs = serve_strs + "\n" + (len(s) * "-") + "\n"
                for studnum, rank in self.group_members[grpnum]:
                    student = self._students.get(studnum)
                    teamstr = ""
                    if studnum in team_partners:
                        partner, accepted = team_partners[studnum]
                        teamstr = (
                            " | team with "
                            + self._students.get(partner).sid
                            + (" accepted" if accepted else " rejected")
                        )
                    s =(
                        "{0:<{4}} {1:<{5}} | {2:<{6}} | preference {3}{7}".format(
                            student.lastname + ",",
                            student.firstname,
                            student.sid,
                            rank,
                            lastname_maxlen + 1,
                            firstname_maxlen,
                            sid_maxlen,
                            teamstr,
                        )
                    )
                    print(s)
                    serve_strs = serve_strs + s + "\n"
                    p1 = (
                        p
                        + "{0:<{4}}, {1:<{5}} , {2:<{6}} , preference {3}{7} \n".format(
                            student.lastname,
                            student.firstname,
                            student.sid,
                            rank,
                            lastname_maxlen + 1,
                            firstname_maxlen,
                            sid_maxlen,
                            teamstr,
                        )
                    )
                    serve_strp = serve_strp + p1
                    outfile.write(p1)
                print()
        return serve_strp, serve_strs
    
    def print_statistics(self):
        self.solve()
        serve_str = ""
        for rank in range(len(self.group_sizes)):
            cost = rank_to_cost(rank, len(self.group_sizes))
            fulfilled = self.preferences_fulfilled[rank][0]
            padded = self.preferences_fulfilled[rank][1]
            print(
                "Cost of preference {:2}:       {:5}, "
                "accepted {:3} of which {:3} padded".format(
                    rank, cost, fulfilled, padded
                )
            )
            serve_str = serve_str +"Cost of preference {:2}:       {:5},accepted {:3} of which {:3} padded\n".format(
                    rank, cost, fulfilled, padded) 
        if disfavoured_cost:
            print(
                "Cost of disfavoured matches: {:5}, accepted {:3}".format(
                    disfavoured_cost, self.preferences_fulfilled["disfavoured"][0]
                )
                
            )
            serve_str = serve_str +"Cost of disfavoured matches: {:5}, accepted {:3}\n".format(
                    disfavoured_cost, self.preferences_fulfilled["disfavoured"][0]
                )
        print(
            "Team bonus per student: {:10}, accepted {:3} of {} teams".format(
                team_bonus, len(self.accepted_teams), len(self._students.verified_teams)
            )
        )
        serve_str = serve_str +"Team bonus per student: {:10}, accepted {:3} of {} teams\\n\n".format(
                team_bonus, len(self.accepted_teams), len(self._students.verified_teams))
        print()
        print("Number of students:", len(self._students))
        serve_str = serve_str + f"Number of students: {len(self._students)}\n"
        print("Minimised cost:", self.solver.Objective().Value())
        serve_str = serve_str +f"Minimised cost:{self.solver.Objective().Value()}\n\n\n"
        print()
        print(
            "{} variables, {} constraints".format(
                self.solver.NumVariables(), self.solver.NumConstraints()
            )
        )
        serve_str = serve_str +"{} variables, {} constraints\n".format(
                self.solver.NumVariables(), self.solver.NumConstraints()
            )
        print(
            "{} ms, {} iterations, {} branch-and-bound nodes".format(
                self.solver.wall_time(), self.solver.iterations(), self.solver.nodes()
            )
        )
        serve_str = serve_str +"{} ms, {} iterations, {} branch-and-bound nodes\n".format(
                self.solver.wall_time(), self.solver.iterations(), self.solver.nodes()
            )
        
        return serve_str


def mapgoups_conv(spec):
    """Parse the group map command line option.
    Format: A:B[,C:D...] to map group A to B (C to D, ...).
    """
    gmap = {}
    for gm in spec.split(","):
        gm_from_to = gm.split(":", 1)
        if len(gm_from_to) != 2:
            raise argparse.ArgumentTypeError("Invalid group map: " + gm)
        gmap[gm_from_to[0].strip()] = gm_from_to[1].strip()
    return gmap


def groups_conv(spec):
    """Parse the groups command line option.
    A[:n[:m]][,B[:p[:q]]...][,-] to define groups A (B)
    of minimal size n (p) and maximal size m (q).
    Adding a group named "-" finalises the groups, i.e. no more groups
    can be defined (to prohibit auto groups definition from preferences).
    """
    seen = set()
    groups_def = []
    other_min_max = (None, None)
    final = False
    for gs in spec.split(","):
        name_sizes = gs.split(":", 2)
        name = name_sizes[0].strip()
        if name == "-":
            final = True
        if not name:
            raise argparse.ArgumentTypeError("Empty group name is not allowed")
        group_def = [name, None, None]
        if len(name_sizes) >= 2 and name_sizes[1].strip():
            try:
                min_size = int(name_sizes[1])
            except ValueError:
                raise argparse.ArgumentTypeError(
                    "Invalid minimal group size: " + name_sizes[1]
                ) from None
            group_def[1] = min_size
        if len(name_sizes) == 2:
            group_def[2] = min_size
        elif len(name_sizes) >= 3 and name_sizes[2].strip():
            try:
                max_size = int(name_sizes[2])
            except ValueError:
                raise argparse.ArgumentTypeError(
                    "Invalid maximal group size: " + name_sizes[2]
                ) from None
            group_def[2] = max_size
            if not group_def[1] is None and group_def[1] > max_size:
                raise argparse.ArgumentTypeError(
                    "Minimal group size must not be larger than maximal size."
                )
        if group_def[0] == "*":
            other_min_max = (group_def[1], group_def[2])
        else:
            groups_def.append(group_def)
    return groups_def, other_min_max, final


def groups_file_conv(filename):
    """
    Parse the csv file with groups options in
    Takes
        filename which is file with group info in

    Returns
        groups_def,
        other_min_max,
        final
    """
    # Check that file exists
    if os.path.isfile(filename):
        # file exists, crack on
        print("Using Group Option File")
        # open file
        with open(filename, "r") as infile:
            contents = infile.read()
            opstring = ""
            contents = contents.split("\n")
            for line in contents:
                opstring = opstring + line.replace(",", "")
    else:
        print("Group Option File does not exist")


def int_gt_0(spec):
    min_prefs = int(spec)
    if min_prefs <= 0:
        raise argparse.ArgumentTypeError("min_prefs must be >0.")
    return min_prefs


parser = argparse.ArgumentParser(
    formatter_class=argparse.RawTextHelpFormatter,
    description="Assign students to groups according to their preferences.",
    epilog="Preferences file format: one line per student of the form\n"
    "last name ; first name ; student ID ; grp1|grp2|... "
    "[; team partner student ID]\n"
    "The ID of each student must be unique. The prefenence list "
    "(grp1|grp2|...)\nis a list of the names of the preferred groups "
    "(higher preferences earlier),\nseparated by |. "
    "Team partners are optional.\n"
    "Comments are allowed following # until the end of the line.",
)

parser.add_argument(
    "preferences_file",
    type=str,
    help="The file with the student names and IDs,\n"
    "their group preferences and optionally team partners.\n"
    "See below for the file format.",
)

parser.add_argument(
    "-m",
    "--min_prefs",
    type=int_gt_0,
    default=pad_if_fewer_prefs_than,
    help="If a student gives fewer than MIN_PREFS preferences,\n"
    "treat all other groups as next lower preference\n"
    'instead of "disfavoured" (default: {}).\nSet to 1 to disable.'.format(
        pad_if_fewer_prefs_than
    ),
)

parser.add_argument(
    "-d",
    "--disfavoured_cost",
    type=int,
    default=disfavoured_cost,
    help="The cost for assigning a student to a group that is\n"
    "not in his/her preference list (default: {}).\n"
    "The value 0 forbids disfavoured assignments\n"
    "(may lead to unsolvable assignment problems).".format(disfavoured_cost),
)

parser.add_argument(
    "-t",
    "--team_bonus",
    type=float,
    default=team_bonus,
    help="Reduce the cost of assigning both team members to the\n"
    f"same group by this for both members (default: {team_bonus}).",
)

parser.add_argument(
    "-c",
    "--cost_function",
    type=str,
    default="square",
    choices=["linear", "square", "exp", "decell"],
    help="The function to calculate the cost from the rank\n" "(default: square).",
)

parser.add_argument(
    "-r",
    "--map_groups",
    type=mapgoups_conv,
    default={},
    help="A:B merges group A into B. This eliminates group B,\n"
    "assigning all its slots to A. Use a comma separated list\n"
    "for more than one mapping.",
    metavar="GROUP_MAP",
)

parser.add_argument(
    "-g",
    "--groups",
    type=groups_conv,
    default=([], (None, None), False),
    help='A comma separated list of group names.\nIf a group of the name "-" '
    "is given, no other groups\nwill be defined automatically. "
    "Otherwise, groups with\nall names appearing in the students "
    "preferences will be\ndefined automatically (beware of typos!).\n"
    "Optionally, group sizes can be provided. Examples:\n"
    "A:6 to give group A the size 6,\n"
    "A:6:8 to give group A min size 6 and max size 8,\n"
    "A:6: to only give the min size,\n"
    "A::8 to only give the max size \n"
    "(these are the sizes before applying group mappings).\n"
    'Use the group name "*" to set min/max sizes for all\n'
    "groups whose sizes are not given explicitly.\n"
    "Sizes of all groups which are not specified in this way\n"
    "will be determined automatically.",
)

parser.add_argument(
    "-gf",
    "--groupfile",
    type=str,
    default="",
    help="A comma separated file of group names.\n"
    "each row contains one group and its limits.\n"
    "If given with option -g it will overide the argument given with -g",
)

parser.add_argument(
    "--debug",
    action="store_true",
    help="Enable debug mode to print a backtrace in case of an input error.",
)

parser.add_argument(
    "-s",
    "--streamfile",
    type=bool,
    default=False,
    help="If set to True will stream the standard out to the file out.txt\n"
    "Note that this will append rather than overwrite the file",
)

if __name__ == "__main__":
    args = parser.parse_args()
    # set global variables, overriding default values
    pad_if_fewer_prefs_than = args.min_prefs
    disfavoured_cost = args.disfavoured_cost
    if not disfavoured_cost:
        print(
            "Warning: Disfavoured matches are disabled. "
            "This may lead to unsolvable assignment problems."
        )
    team_bonus = args.team_bonus
    cost_function = args.cost_function
    # other options
    # check if group file given
    if args.groupfile != "":
        # As file present will parse to get
        # user_group_def, othermin_max, group_def_final
        print("File")
        print(args.groupfile)
        groups_file_conv(args.groupfile)

    user_group_def, other_min_max, group_def_final = args.groups
    map_groups = args.map_groups

    try:
        groups = Groups(user_group_def, other_min_max, group_def_final)
        students = Students.parse(args.preferences_file, groups)
        solver = Solver(students, map_groups)
        print()
        solver.print_result()
        solver.print_statistics()
    except InputError as err:
        if args.debug:
            raise
        else:
            print("Error:", err)
