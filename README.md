
StuGrAssi -- Student-Group Assignment with Preferences
=======================================================

A Python program/module to assign students to groups according to their preferences.

#### Usage

Can be run as a simple Flask server (run the main.py script)

or as a local script with command line arguments (run the stugrassi.py script)

Flask is still work in progress, the correct results are producted but the formatting and output needs improvement.

Also Flask server runs in a unsafe mode, for proper usage make sure it is secure https://flask.palletsprojects.com/en/3.0.x/deploying/index.html


#### The Problem

* There are *N* students and *G* groups.
* Every student provides a ranked list with his/her preferred (not necessarily all) groups.
  A certain number of preferences should be given by a student, and there must be no advantage for students who provide fewer preferences.
* Every student must be assigned to exactly one group.
* Each group has a minimal and a maximal size.
  The sizes can be determined automatically so that the same number of students is assigned to each group (differing only by 1 if the *N* is not a multiple of *G*).
  Optionally, the min/max sizes of groups can be set manually and independently for individual or all groups.
* Students may ask to be assigned to the same group as one other student of their choice.
  But this should only be done if it doesn't lead to a disadvantage for others.
* The groups do not have preferences for students, i.e. all students are treated as equal.

This is a typical problem that occurs when a large number of students must be allocated to smaller exercise classes in a fair and non-discriminatory way.
I.e. neither the grades of the students nor personal preferences of the tutors are taken into account.

The problem is similar to the **"hospital-residents problem"** (HR) and the **"student allocation problem"** (SA). Major differences:

* In the HR, not only are the hospitals ranked by the residents, but also vice versa.
* In the SA, supervisors are introduced that may offer several projects (groups), but have a limited total capacity. Supervisors rank the students that prefer one of their projects.

#### Optimization Strategy

The problem is solved by defining a cost function and minimising it under certain constraints with linear optimization.

Assigning a student to a group increases the cost depending on the position of this group in the student's preference list ("rank").
The higher the preference, the lower the assignment cost (see the option `--cost_function`).
In case two team partners are assigned to the same group, the cost is decreased by a small amount (see the option `--team_bonus`).

*Constraints:*

* Each student is assigned to exactly one group.
* Each group has at least as many members as its minimal size and at most as many as its maximal size (see the option `--groups`).

Note that usually there will be more than one optimal solution.
The program will provide only one of them.

#### License

2-clause BSD

#### Dependencies

* Python 3

* Google's OR-Tools (https://developers.google.com/optimization).
  Can be installed with pip3:

  `pip3 [--user] install [--upgrade] ortools`

#### GitLab page
For original Fork
https://gitlab.com/maiphi/student-group-assignment
For this Fork
https://gitlab.com/mParkesUCL/student-group-assignment

Usage
-----

`stugrassi.py [-h] [-m MIN_PREFS] [-d DISFAVOURED_COST] [-t TEAM_BONUS] [-c {linear,square,exp,decell}] [-r GROUP_MAP] [-g GROUPS] [--debug] preferences_file`

#### Positional arguments

* `preferences_file`

  The file with the student names and IDs, their group preferences and, optionally, team partners.

  File format: one line per student of the form

  `last name ; first name ; student ID ; grp1|grp2|... [; team partner student ID]`

  The ID of each student must be unique.
  The preference list (grp1|grp2|...) is a list of the names of the preferred groups (higher preferences earlier), separated by |.
  Team partners are optional.
  Comments are allowed following # until the end of the line.

  Each student can provide at most one team partner.
  Teams are only considered if the partners choose each other mutually.
  If the team bonus is small, teams are only formed if the obtained solution is also optimal without applying the team bonus
  (specifying a partner means "I would like to be in the same group as ... if possible").

#### Optional arguments

* `-h, --help`

  Show the help message and exit.

* `-m MIN_PREFS, --min_prefs MIN_PREFS`

  The minimal number of preferences a student should provide (default: 3).

  If a student gives fewer than MIN_PREFS preferences, treat all other groups as next lower preference instead of "disfavoured".
  This prevents students to get an advantage by providing fewer preferences.
  Set this to 1 to disable.

* `-d DISFAVOURED_COST, --disfavoured_cost DISFAVOURED_COST`

  The cost for assigning a student to a group that is not in his/her preference list (default: 1000).
  The value 0 forbids disfavoured assignments.
  Note that this may lead to unsolvable assignment problems.

* `-t TEAM_BONUS, --team_bonus TEAM_BONUS`

  Reduce the cost of assigning both team members to the same group by this for both members (default: 0.001).

* `-c {linear,square,exp,decell}, --cost_function {linear,square,exp,decell}`

  The function to calculate the assignment cost from the rank (default: square).
  Possible choices:

  - `linear`: *cost = rank* → 0, 1, 2, 3, 4, ...

  - `square`: *cost = rank\*(rank+1)/2* → 0, 1, 3, 6, 10, ...

  - `exp`: *cost = 2^rank* → 1, 2, 4, 8, 16, ...

  - `decell`: decreasing difference (depends on the number of groups), e.g. for 5 groups → 0, 4, 7, 9, 10.

* `-r GROUP_MAP, --map_groups GROUP_MAP`

  GROUP_MAP=A:B merges group A into B. This eliminates group B, assigning all its slots to A.
  Use a comma separated list for more than one mapping.

  Use case:
  The students choose time slots and slot A is very unpopular, while slot B is very popular.
  You may then cancel slot A and increase the number of participants in slot B accordingly by mapping A to B.

* `-g GROUPS, --groups GROUPS`

  A comma separated list of group names and, optionally, sizes.
  If a group of the name "-" is given, no other groups will be defined automatically.
  Otherwise, groups with all names appearing in the students preferences will be defined automatically (beware of typos in the preferences file!).
  Optionally, minimal and/or maximal group sizes can be provided.
  Use the group name "*" to set min/max sizes for all groups whose sizes are not given explicitly. Sizes of all groups which are not specified in this way will be determined automatically.

  *Examples:*

  - `"A,B,C,-"` to define 3 groups with names A, B, C, and no others,
  - `"A:6"` to give group A the size 6 (i.e. min and max),
  - `"A:6:8"` to give group A min size 6 and max size 8,
  - `"A:6:"` to only give the min size 6 and determine the max size automatically,
  - `"A::8"` to only give the max size 8 and determine the min size automatically,
  - `"A:5:,*:10:"` give group A min size 5 and min size 10 to all other groups.

  Note that these are the sizes before applying group mappings with `--map_groups`.

* `--debug`

  Enable debug mode to print a backtrace in case of an input error.

## Examples

The file `example.csv` contains a real-world example with (anonymous, of course) preference data from a course at a university.
We had to assign 67 students to 7 groups (exercise classes), simply named "1", ..., "7".

Do everything automatically:

`stugrassi.py example.csv`

In our case, group 5 was in English and all others in German.
We decided to only assign those students to the english group that gave it as their first or second choice (if this reduces the matching cost) by setting the minimal and maximal sizes of group 5 to 3 and 4, respectively:

`stugrassi.py --groups 5:3:4 example.csv`

Since the time slot for group 6 was very unpopular (4 first choices), and group 1 had twice as many first choices as slots (20 in total), we decided to move group 6 to the same time slot as group 1:

`stugrassi.py --groups 5:3:4 --map_groups 6:1 example.csv`

Note that afterwards we had to split group 1 back into two groups, taking care that the teams are not broken up.
This has not been automated, but it is trivial to do by hand.

## Python module

Using this as a Python module is possible, but you'll have to consult the source code yourself for now. Looking at the few lines of code in the `__main__` part should be enough to get you started.
