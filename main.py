from distutils.log import debug
from fileinput import filename

# from flask import *

import flask as flk

import stugrassi as stu

app = flk.Flask(__name__)


@app.route("/")
def main():
    return flk.render_template("Index.html")


@app.route("/success", methods=["POST"])
def success():
    if flk.request.method == "POST":
        f = flk.request.files["file"]
        filename = f.filename #on server replace with full path so permissions can be set
        f.save(filename)
        gstr = flk.request.form.get("gstring")
        dstr = flk.request.form.get("setd")
        #print(f"disfavour = {dstr}")
        ostring,opstring,osstring = runOpt(filename, gstr, dstr)
        ostring = ostring.split('\n')
        opstring = opstring.split('\n')
        osstring = osstring.split('\n')
        print("DONE")
        #download("./out.csv")
        return flk.render_template(
            "Acknowledgement.html", name=f.filename, gstring=gstr, ostring= ostring, opstring=opstring, osstring=osstring
        )

# @app.route('<path:/ofilename>', methods=['GET', 'POST'])
# def download(ofilename):
#     # Appending app path to upload folder path within app root folder
#     #uploads = os.path.join(current_app.root_path, app.config['UPLOAD_FOLDER'])
#     # Returning file from appended path
#     return flk.send_from_directory(filename=ofilename)

def runOpt(preffilename, gstring, dstring):
    '''Function to run the solver

    Args:
        preffilename (str): The filename containing the preferences
        gstring (str): The string for the g option
        dstring (str): The disfavour cost

    Returns:
        _type_: _description_
    '''
    stu.cost_function = "square"
    stu.disfavoured_cost = int(dstring)
    stu.pad_if_fewer_prefs_than = 3
    stu.team_bonus = 0.001
    stu.minimal_maxsize_increment = 1
    stu.create_solver_args = ["CBC"]
    try:
        if int(stu.ortools_version.split(".")[0]) < 8:
            stu.create_solver_args = ["student_group_assignment"] + stu.create_solver_args
    except:
        # Just use the default.
        pass
    # solver_opts = {
    #     "disfavoured_cost": 1000,
    #     "pad_if_fewer_prefs_than": 3,
    #     "team_bonus": 0.001,
    #     "cost_function": "square",
    # }
    # go through group file
    print(f"Converting {preffilename}")
    # stu.groups_file_conv(gfilename)

    print(f"The Input g string was \n {gstring}, splits into: \n")
    user_group_def = parse_gstring(gstring)
    print(f"The disfavour cost was \n {dstring}")
    other_min_max = (None, None)
    group_def_final = False
    map_groups = {}

    try:
        groups = stu.Groups(user_group_def, other_min_max, group_def_final)
        students = stu.Students.parse(preffilename, groups)
        solver = stu.Solver(students, map_groups)
        print()
        outstringp, outstrings = solver.print_result()
        outstring = solver.print_statistics()
        return outstring, outstringp, outstrings
    except stu.InputError as err:
        print("Error:", err)
        outstring = f"Error: {err}"
        return outstring, "", ""


def parse_gstring(gstring):
    supers = gstring.split(",")
    suplist = []
    for sup in supers:
        final = sup.split(":")
        if len(final) == 3:
            lead = []
            lead.append(final[0])
            lead.append(int(final[1]))
            lead.append(int(final[2]))

            suplist.append(lead)

    print(suplist)
    return suplist


# def chkG(gstring):

#     if len(gstring) > 0:


if __name__ == "__main__":
    #swap comments below when running properly
    app.run(debug=True)
    #app.run(host = '0.0.0.0')
